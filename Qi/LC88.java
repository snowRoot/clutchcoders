// 88. Merge Sorted Array
// Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

// Note:
// You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.

import java.util.*;

public class LC88{
	public void merge(int[] nums1, int m, int[] nums2, int n){ // this is essentially two pointer approach, same as LC349 LC350
		int i = m - 1;
        int j = n - 1;
        int k = m + n -1;
        while(i >= 0 && j >=0){ // for &&, if the first condition is not ture, the second won't be evaluated
            if( nums1[i] > nums2[j]){
                nums1[k--] = nums1[i--]; 
            }else{
                nums1[k--] = nums2[j--]; 
            }
        }
        
        while(j >= 0){ // so here only j could be larger than 0
            nums1[k--] = nums2[j--];
        }
	}

	public void merge2(int[] nums1, int m, int[] nums2, int n) {// I'm pretty sure this solution is not allowed in a real interview, even though it's accepted. Because it has no thinking process
        // int i = 0;
        // while(i < n){
        //     nums1[m + i] = nums2[i]; 
        //     i++;
        // }
        System.arraycopy(nums2, 0, nums1, m, n); //just concatenate two array
        Arrays.sort(nums1); // do a sort
    }

	public static void main(String [] args){
		int m = 4;
		int n = 3;
		int [] nums1 = new int[10];
		for(int i = 0; i < nums1.length; i++){
			nums1[i] = Integer.MAX_VALUE;
		}
		nums1[0] = 1;
		nums1[1] = 4;
		nums1[2] = 6;
		nums1[3] = 10;
		int [] nums2 = {3, 4, 7};
		LC88 l = new LC88();
		l.merge(nums1, m, nums2, n);
		System.out.println(Arrays.toString(nums1));
	}
}