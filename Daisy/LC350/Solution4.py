class Solution(object):
    def intersect(self, nums1, nums2):
        ans = []
        for n in nums1:
            if n in  nums2:
                ans.append(nums2.pop(nums2.index(n)))
        return ans
