package LC144;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lipingzhang on 3/31/17.
 */

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}

public class Solution1 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();

        if(root == null){
            return ans;
        }

        helper(root, ans);
        return ans;
    }

    private void helper(TreeNode root, List<Integer> ans){
        ans.add(root.val);
        if(root.left != null){
            helper(root.left, ans);
        }
        if(root.right != null){
            helper(root.right, ans);
        }
    }
}
